﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TaskProcessor;

namespace TaskMaker
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            var serviceProvider = host.Services;

            var config = serviceProvider.GetService<IConfiguration>();

            var makerSettings = config?.GetSection("Clients").Get<List<TaskMakerConfiguration>>();
            if (makerSettings == null)
                return;

            var makerTasks = new List<Task>();

            foreach (var settings in makerSettings)
            {
                var maker = serviceProvider.GetService<ITaskMaker>();

                if (maker == null)
                    continue;

                maker.Configuration = settings;
                makerTasks.Add(Task.Run(() => CreateTasks(maker)));
            }

            await Task.WhenAll(makerTasks);
        }


        private static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args)
            .ConfigureServices((context, provider) =>
            {
                provider.AddSingleton<IProcessorConfiguration, ProcessorConfiguration>();
                provider.AddSingleton<ITaskProcessor, TaskProcessor.TaskProcessor>();
                provider.AddTransient<ITaskMaker, TaskMaker>();
            });


        private static async Task CreateTasks(ITaskMaker maker)
        {
            await maker.CreateTask();
        }
    }
}
