﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TaskProcessor;
using TaskProcessor.ProcessorExceptions;

namespace TaskMaker
{
    public class TaskMaker : ITaskMaker
    {
        private readonly ITaskProcessor _processor;
        public TaskMakerConfiguration Configuration { get; set; }

        public TaskMaker(ITaskProcessor processor)
        {
            _processor = processor;
        }

        public async Task CreateTask()
        {
            var tasks = new List<Task<int>>();
            var rnd = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < 10; i++)
            {
                //simulate task processing time using random delay
                //delay - task execution time
                //internal delay - _processor.Process delay before execution

                var i1 = i;

                await Task.Delay(Configuration.InternalDelay);

                var function = CreateFunctionWithToken(i1);

                var tokenSource = new CancellationTokenSource();
                if (Configuration.LimitExecutionTime)
                {
                    tokenSource.CancelAfter(Configuration.WaitingTime);
                }
                
                try
                {
                    var task = _processor.EnqueueTask(function, Configuration.Id.ToString(), i1.ToString(), 1, tokenSource.Token);

                    var result = await task;
                    Console.WriteLine(result);
                }
                catch (ProcessorOverflowException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Catch exception in task maker");
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    tokenSource.Dispose();
                }
            }
        }

        private Func<int, CancellationToken, int> CreateFunctionWithToken(int operationId)
        {
            int Function(int arg, CancellationToken token)
            {
                if (Configuration.ThrowException) throw new Exception("Exception message");

                Console.WriteLine("Made task number {0} ||| Created by {1}", operationId, Configuration.Id);

                for (int i = 0; i < 3; i++)
                {
                    if (token.IsCancellationRequested)
                        return -1000;

                    Task.Delay(1000).Wait();
                    Console.WriteLine(i);
                }

                return arg * 2;
            }

            return Function;
        }
    }
}
