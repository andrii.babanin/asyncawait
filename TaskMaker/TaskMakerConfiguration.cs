﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskMaker
{
    public class TaskMakerConfiguration
    {
        public int Id { get; set; }
        public int InternalDelay { get; set; }
        public int ExecutionTime { get; set; }
        public int WaitingTime { get; set; }
        public bool LimitExecutionTime { get; set; }
        public bool ThrowException { get; set; }
    }
}
