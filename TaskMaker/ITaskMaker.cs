﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TaskMaker
{
    public interface ITaskMaker
    {
        TaskMakerConfiguration Configuration { get; set; }
        Task CreateTask();
    }
}
