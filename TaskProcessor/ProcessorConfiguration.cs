﻿using Microsoft.Extensions.Configuration;
using System;

namespace TaskProcessor
{
    public class ProcessorConfiguration : IProcessorConfiguration
    {
        private readonly IConfigurationSection _config;
        public ProcessorConfiguration(IConfiguration config)
        {
            _config = config.GetSection("TaskProcessor");
        }

        public int MaxWaitingTime  => Convert.ToInt32(_config["MaxWaitingTime"] ?? "5000");
        public int MaxQueueCapacity  => Convert.ToInt32(_config["MaxQueueCapacity"] ?? "100");
    }
}
