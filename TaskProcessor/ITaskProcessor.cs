﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TaskProcessor
{
    public interface ITaskProcessor
    {

        /// <summary>
        ///  Create task from passed function and enqueue it to internal collection.
        /// </summary>
        /// <typeparam name="TArg">Type of function argument</typeparam>
        /// <typeparam name="TResult">Type of function return value</typeparam>
        /// <param name="func">Function to execute</param>
        /// <param name="makerId">Task maker`s id</param>
        /// <param name="taskId">Task id to identify the function when executing</param>
        /// <param name="argument">Function argument</param>
        /// <param name="token">Cancellation token</param>
        /// <returns>Task that represents a pending operation.</returns>
        Task<TResult> EnqueueTask<TArg, TResult>(Func<TArg, CancellationToken, TResult> func, string makerId,
            string taskId, TArg argument, CancellationToken token);
    }
}
