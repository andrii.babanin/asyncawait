﻿using System.Threading;
using System.Threading.Tasks;

namespace TaskProcessor
{
    internal class OperationInfo
    {
        public Task Task { get; set; }
        public string MakerId { get; set; }
        public string TaskId { get; set; }
        public CancellationToken Token { get; set; }
    }
}
