﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using TaskProcessor.ProcessorExceptions;

namespace TaskProcessor
{
    public class TaskProcessor : ITaskProcessor
    {
        //Queue settings
        private readonly IProcessorConfiguration _configuration;
        private readonly ConcurrentQueue<OperationInfo> _operationQueue;
        private readonly object _lock = new object();

        //1 - run
        //0 - stop
        private volatile int _isQueueProcessing;

        public TaskProcessor(IProcessorConfiguration configuration)
        {
            _configuration = configuration;
            _isQueueProcessing = 0;
            _operationQueue = new ConcurrentQueue<OperationInfo>();

            StartProcessing();
        }

        public Task<TResult> EnqueueTask<TArg, TResult>(Func<TArg, CancellationToken, TResult> func,
            string makerId,
            string taskId,
            TArg argument,
            CancellationToken token)
        {
            if (_operationQueue.Count == _configuration.MaxQueueCapacity)
                throw new ProcessorOverflowException("Task limit exceeded");


            var task = new Task<TResult>(() => PerformTask(func, argument, token), token);
            var operation = new OperationInfo()
            {
                MakerId = makerId,
                TaskId = taskId,
                Task = task,
                Token = token
            };

            _operationQueue.Enqueue(operation);

            //Start processing if its stopped
            if(_isQueueProcessing == 0)
                StartProcessing();

            return task;
        }

        private void StartProcessing()
        {
            lock (_lock)
            {
                if (_isQueueProcessing == 0)
                {
                    _isQueueProcessing = 1;
                    Task.Run(ProcessQueue);
                }
            }
           
        }
        private void ProcessQueue()
        {
            while (true)
            {
                if (!_operationQueue.TryDequeue(out var operation))
                {
                    _isQueueProcessing = 0;
                    return;
                }

                Console.WriteLine("\nStart processing task | {0} | created by {1}", operation.TaskId, operation.MakerId);

                try
                {
                    operation.Token.ThrowIfCancellationRequested();
                    operation.Task.Start();
                    operation.Task.Wait(operation.Token);
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Operation | by {0} | has been aborted", operation.MakerId);
                }

            }
        }

        /// <summary>
        /// Use this method to pass it in Task[TResult] constructor
        /// </summary>
        private TResult PerformTask<TArg, TResult>(Func<TArg, CancellationToken, TResult> func, TArg argument, CancellationToken token)
        {
            var result = func.Invoke(argument, token);
            return result;
        }
    }
}
