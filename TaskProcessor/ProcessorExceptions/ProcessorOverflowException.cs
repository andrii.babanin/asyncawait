﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskProcessor.ProcessorExceptions
{
    public class ProcessorOverflowException : Exception
    {
        public ProcessorOverflowException() {}
        public ProcessorOverflowException(string message) : base(message) {}
        public ProcessorOverflowException(string message, Exception innerException) : base(message, innerException){}
    }
}
