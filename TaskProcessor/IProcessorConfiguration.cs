﻿namespace TaskProcessor
{
    public interface IProcessorConfiguration
    {
        int MaxWaitingTime { get; }
        int MaxQueueCapacity { get; }
    }
}
